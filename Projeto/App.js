import React from 'react';
import { TextInput, View, Button, StyleSheet, TouchableOpacity } from 'react-native';

export default function Login(){

  const [usernameText, setUsernameText] = React.useState('');
  const [senhaText, setSenhaText] = React.useState('');
  const [isLogged, setIsLogged] = React.useState('');

  let username = "admin";
  let senha = "admin";

  function login(){
    if(usernameText===username && senhaText===senha)
    {
      setIsLogged(true);
    }
    else
    {
      setIsLogged(false);
    }
  } 

    return (
      <View style={styles.container}>
        <TextInput
          style={styles.inputsLogin}
          placeholder="E-mail"
          onChangeText={(text) => setUsernameText({ text })}
          value={usernameText}
        />
        <TextInput
          style={styles.inputsLogin}
          placeholder="Senha"
          onChangeText={(text) => setSenhaText({ text })}
          value={senhaText}
        />
        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => {}}  >
          <Text >Button</Text>
        </TouchableOpacity>
        <Button
          style={styles.loginButton}
          title="ENTRAR"
          color="#C2C2C2"
          onPress={login}
        />
        <Button
          style={styles.esqueciSenhaButton}
          title="ESQUECI A MINHA SENHA"
          color="red"
        />
      </View>
    );
}

const styles = StyleSheet.create({  
  container: {  
    display: "flex", 
    justifyContent: "center", 
    alignItems: "center", 
    height: "100%", 
    width: "100%"  
  },

  inputsLogin: {
    height: 40,
    width: 300, 
    borderColor: 'gray', 
    borderWidth: 1,
    margin: 4
  },

  loginButton: {
    height: 40, 
    width: 300, 
    margin: 20
  },

  esqueciSenhaButton: {
    height: 40, 
    width: 300, 
    margin: 20,
    color:"blue"
  }
});